# League of Legends high diamond winning stats in the 10 first minutes

Stats of 10 first minutes of the winning team in high diamond elo.

## Data

[Link](https://www.kaggle.com/bobbyscience/league-of-legends-diamond-ranked-games-10-min) to the data.
