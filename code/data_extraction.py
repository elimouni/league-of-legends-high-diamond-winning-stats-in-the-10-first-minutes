import data_exportation as dexp
import numpy as np

# Function to check if a key exists in a list
def checkKey(dict, key): 
    if key in dict: 
        return 1
    else: 
        return 0 

# Function to recognize a letter written on a picture and to return it as a char
def get_data():
    # Reading the data contained in the CSV file
    result = dexp.read_data_raw()

    # Making simple observations about the data
    print("--------------- Diamond I League of Legends statistics BEFORE the 10th minute ---------------\n\n")

    # W : winner / L : loser
    W_kills = 0
    L_kills = 0
    W_wards_placed = 0
    L_wards_placed = 0
    W_wards_destroyed = 0
    L_wards_destroyed = 0
    W_gold = 0
    L_gold = 0
    W_tower_destroyed = 0
    L_tower_destroyed = 0
    W_deaths = 0
    L_deaths = 0
    W_first_blood = 0
    L_first_blood = 0
    W_level = 0
    L_level = 0
    W_farm = 0
    L_farm = 0
    W_farm_jungle = 0
    L_farm_jungle = 0
    W_farm_per_min = 0
    L_farm_per_min = 0
    W_heralds = 0
    L_heralds = 0
    W_drakes = 0
    L_drakes = 0
    number_matches = 0

    for row in result:
        if(int(row[1])): # If blue team wins
            W_kills += int(row[5])
            W_wards_placed += int(row[2])
            W_wards_destroyed += int(row[3])
            W_gold += int(row[12])
            W_tower_destroyed += int(row[11])
            W_deaths +=int(row[6])
            W_first_blood += int(row[4])
            W_level += float(row[13])
            W_farm += int(row[15])
            W_farm_jungle += int(row[16])
            W_farm_per_min += float(row[19])
            W_drakes += int(row[9])
            W_heralds += int(row[10])

            L_kills += int(row[24])
            L_wards_placed += int(row[21])
            L_wards_destroyed += int(row[22])
            L_gold += int(row[31])
            L_tower_destroyed += int(row[30])
            L_deaths += int(row[25])
            L_first_blood += int(row[23])
            L_level += float(row[32])
            L_farm += int(row[34])
            L_farm_jungle += int(row[35])
            L_farm_per_min += float(row[38])
            L_drakes += int(row[28])
            L_heralds += int(row[29])

        else: # If red team wins
            W_kills += int(row[24])
            W_wards_placed += int(row[21])
            W_wards_destroyed += int(row[22])
            W_gold += int(row[31])
            W_tower_destroyed += int(row[30])
            W_deaths += int(row[25])
            W_first_blood += int(row[23])
            W_level += float(row[32])
            W_farm += int(row[34])
            W_farm_jungle += int(row[35])
            W_farm_per_min += float(row[38])
            W_drakes += int(row[28])
            W_heralds += int(row[29])

            L_kills += int(row[5])
            L_wards_placed += int(row[2])
            L_wards_destroyed += int(row[3])
            L_gold += int(row[12])
            L_tower_destroyed += int(row[11])
            L_deaths += int(row[6])
            L_first_blood += int(row[4])
            L_level += float(row[13])
            L_farm += int(row[15])
            L_farm_jungle += int(row[16])
            L_farm_per_min += float(row[19])
            L_drakes += int(row[9])
            L_heralds += int(row[10])

        number_matches+=1

    # Statistics calculation based on collected data
    print("Study made using the data of " + str(number_matches) + " games.\n\n")

    print("Average more kills made by the winning team  : " + str(round(100 * (W_kills - L_kills) / L_kills, 2)) + "% (" + str(round(W_kills/number_matches, 2)) +" vs " + str(round(L_kills/number_matches, 2)) + ")")
    print("Average more deaths of the winning team  : " + str(round(100 * (W_deaths - L_deaths) / L_deaths, 2)) + "% (" + str(round(W_deaths/number_matches, 2)) +" vs " + str(round(L_deaths/number_matches, 2)) + ")")
    print("Average more first blood made by the winning team  : " + str(round(100 * (W_first_blood - L_first_blood) / L_first_blood, 2)) + "% (" + str(round(100 * W_first_blood/number_matches, 2)) +"% of the games vs " + str(round(100 * L_first_blood/number_matches, 2)) + "%)\n")

    print("Average more wards placed by the winning team : " + str(round(100 * (W_wards_placed - L_wards_placed) / L_wards_placed, 2)) + "% (" + str(round(W_wards_placed/number_matches, 2)) +" vs " + str(round(L_wards_placed/number_matches, 2)) + ")")
    print("Average more wards destroyed by the losing team : " + str(round(100 * (W_wards_destroyed - L_wards_destroyed) / L_wards_destroyed, 2)) + "% (" + str(round(W_level/number_matches, 2)) +" vs " + str(round(L_level/number_matches, 2)) + ")\n")

    print("Average level advantage of the winning team  : " + str(round(100 * (W_level - L_level) / L_level, 2)) + "% (" + str(round(W_level/number_matches, 2)) +" vs " + str(round(L_level/number_matches, 2)) + ")")
    print("Average gold advantage of the winning team  : " + str(round(100 * (W_gold - L_gold) / L_gold, 2)) + "% (" + str(round(W_gold/number_matches, 2)) +" vs " + str(round(L_gold/number_matches, 2)) + ")")
    print("Average minions advantage of the winning team  : " + str(round(100 * (W_farm - L_farm) / L_farm, 2)) + "% (" + str(round(W_farm_per_min/number_matches, 2)) +" minion per minute vs " + str(round(L_farm_per_min/number_matches, 2)) + ")")
    print("Average jungle mobs advantage of the winning team  : " + str(round(100 * (W_farm_jungle - L_farm_jungle) / L_farm_jungle, 2)) + "% (" + str(round(W_farm_jungle/number_matches, 2)) +" vs " + str(round(L_farm_jungle/number_matches, 2)) + ")\n")

    print("Average more tower destroyed by the winning team  : " + str(round(100 * (W_tower_destroyed - L_tower_destroyed) / L_tower_destroyed, 2)) + "% (" + str(round(W_tower_destroyed/number_matches, 2)) +" vs " + str(round(L_tower_destroyed/number_matches, 2)) + ")")
    print("Average more drakes done by the winning team  : " + str(round(100 * (W_drakes - L_drakes) / L_drakes, 2)) + "% (" + str(round(W_drakes/number_matches, 2)) +" vs " + str(round(L_drakes/number_matches, 2)) + ")")
    print("Average more heralds done by the winning team  : " + str(round(100 * (W_heralds - L_heralds) / L_heralds, 2)) + "% (" + str(round(W_heralds/number_matches, 2)) +" vs " + str(round(L_heralds/number_matches, 2)) + ")")


get_data()