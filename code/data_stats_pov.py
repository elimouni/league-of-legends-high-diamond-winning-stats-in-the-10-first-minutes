import data_exportation as dexp
import numpy as np

# Function to check if a key exists in a list
def checkKey(dict, key): 
    if key in dict: 
        return 1
    else: 
        return 0 

# Function to recognize a letter written on a picture and to return it as a char
def get_data():
    # Reading the data contained in the CSV file
    result = dexp.read_data_raw()

    # Making simple observations about the data
    print("--------------- Diamond I League of Legends impact of some factors BEFORE the 10th minute on the victory of games ---------------\n\n")

    number_matches = 0
    number_matches_with_tower_destruction_diff = 0
    FB_Losing = 0
    FB_Winning = 0
    TowerAdvantage_Losing = 0
    TowerAdvantage_Winning = 0

    for row in result:
        if(int(row[4])): # If blue team has first blood
            if(int(row[1])): # If blue team wins
                FB_Winning += 1
            else:
                FB_Losing += 1
        else: # If red has first blood
            if(int(row[1])): # If blue team wins
                FB_Losing += 1
            else:
                FB_Winning += 1
        if (int(row[11])-int(row[30]) > 0): # If blue team has destroyed more towers
            if(int(row[1])): # If blue team wins
                TowerAdvantage_Winning += 1
            else:
                TowerAdvantage_Losing += 1

            number_matches_with_tower_destruction_diff += 1
        elif (int(row[11])-int(row[30]) < 0): # If red has destroyed more towers
            if(int(row[1])): # If blue team wins
                TowerAdvantage_Losing += 1
            else:
                TowerAdvantage_Winning += 1

            number_matches_with_tower_destruction_diff += 1

        number_matches+=1

    # Statistics calculation based on collected data
    print("Study made using the data of " + str(number_matches) + " games.\n\n")

    print("The team who takes first blood has a " + str(round(100 * FB_Winning / number_matches, 2)) + "% winrate(" + str(FB_Winning) +" vs " + str(FB_Losing) + ")")
    print("The team who destroys more towers has a " + str(round(100 * TowerAdvantage_Winning / number_matches_with_tower_destruction_diff, 2)) + "% winrate(" + str(TowerAdvantage_Winning) +" vs " + str(TowerAdvantage_Losing) + ")")


get_data()