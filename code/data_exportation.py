import csv


# Function to save the data of a letter in the XLSX file
#def save_letter(information):
    # Path to the file
    #filename = "data/result.csv"




# Function to read the data that is in the CSV file
def read_data():
    try:
        # Read the CSV file and put the content in a variable
            with csv.open('../data/campus_recruitment.csv', newline='\n', mode='r') as csv_file:
                csv_reader = csv.DictReader(csv_file)
                line_count = 0
                for row in csv_reader:
                    if line_count == 0:
                        print(f'Column names are {", ".join(row)}')
                        line_count += 1
                    print(f'{row[1]} {row[2]} {row[3]}')
                    line_count += 1
                print(f'Processed {line_count} lines.')
    except FileNotFoundError:
        # If the file doesn't exist then return 0
        return 0
    else:
        # Else return the content of the file
        return csv_reader


def read_data_raw():
    columns = []
    result = []
    counter = 0

    with open('../data/high_diamond_ranked_10min.csv', 'r') as csvfile:
        r = csv.reader(csvfile)
        line = 0
        temp = []
        columns = ""
        total_columns = 0
        counter = 0
        list()
        for row in r:
            line += 1
            if line == 1:
                columns=row
                for element in row:
                    total_columns+=1
            else:
                for element in row:
                    temp.append(element)
                    counter += 1

                line +=1
                counter = 0
                result.append(temp)
                temp = []        

    return result

